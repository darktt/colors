//
//  UIColor+Colors.h
//
//  Created by Darktt on 17/10/12.
//  Copyright (c) 2012 Darktt. All rights reserved.
//

#import <UIKit/UIKit.h>

#define rgb(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define rgba(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a/100]

@interface UIColor (Colors)

// Ouput random color
+ (UIColor *)randomColor;

// Ouput custom color
+ (UIColor *)facebookColor;
+ (UIColor *)twitterColor;
+ (UIColor *)googleRedColor;
+ (UIColor *)googleGreenColor;
+ (UIColor *)googleBlueColor;
+ (UIColor *)googleYellowColor;
+ (UIColor *)yahooRedColor;

// Input hexadecimal string and integer
+ (UIColor *)colorWithHex:(UInt32)hex; // eg: [UIColor colorWithHex:0xff00ff];
+ (UIColor *)colorWithHexString:(NSString *)hex; // eg: [UIColor colorWithHexString:@"ff00ff"];

// Input color name with safe web color names
// Color names is define in ClolorNames.h file
+ (UIColor *)colorWithColorName:(NSString *)name;

// Input RGB value, without alpha value.
+ (UIColor *)colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

// return Color RGB value.
// eg: Red Color, return Red:255,Green:0,Blue:0,Alpha:100
+ (NSDictionary *)colorComponentsFromColor:(UIColor *)color;
- (NSDictionary *)colorComponents;

@end
